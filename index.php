<?php require_once "header.php";  ?>
   
    
    <?php
    
    foreach($links as $group){
    ?>
        <div class="col-xs-6 col-sm-3">
            <?php if(isset($group['site'])) { ?>
            <a class="btn btn-default btn-lg" href="<?=$group['site']['url']?>">
                <span class="glyphicon glyphicon-eye-open"></span>
                <?=$group['site']['title']?>
            </a>
            <?php } ?> 
            <?php if(isset($group['admin'])) { ?>
            <a class="btn btn-default btn-lg" href="<?=$group['admin']['url']?>">
                
                <?=($group['admin']['title'])?$group['admin']['title']:''?>
                <span class="glyphicon glyphicon-briefcase"></span>
            </a>
            <?php } ?> 
        </div>
    <?php 
    }
    ?>
    
<?php require_once "footer.php";  ?>