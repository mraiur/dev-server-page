# Simple Developer server Gate #

I don't like the server folder/file listing so this is a simple gate with bootstrap.

Check the [Tools](https://bitbucket.org/mraiur/dev-server-tools) 

## Preview ##

![dev-gate.png](https://bitbucket.org/repo/eAndx7/images/366485933-dev-gate.png)

## How do I get set up? ##

    Just copy config-example.php to config.php
    Add your projects in $links array
    Add your tools in the $tools_list array

PS. You are more then welcome to fork and share UI changes and ideas or just use it :)